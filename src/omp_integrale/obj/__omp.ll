; ModuleID = '__omp.cpp'
source_filename = "__omp.cpp"
target datalayout = "e-m:e-p:32:32"
target triple = "nuplus-none-none"

%struct.__omp_barrier_t = type { i32 }

; Function Attrs: nounwind uwtable
define i32 @_Z19__omp_get_thread_idv() local_unnamed_addr #0 {
  %1 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2)
  ret i32 %1
}

; Function Attrs: nounwind
declare i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32) #1

; Function Attrs: norecurse nounwind readnone uwtable
define i32 @_Z21__omp_get_num_threadsv() local_unnamed_addr #2 {
  ret i32 4
}

; Function Attrs: nounwind uwtable
define i32 @_Z15__omp_for_starti(i32) local_unnamed_addr #0 {
  %2 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2) #1
  %3 = icmp eq i32 %2, 0
  br i1 %3, label %7, label %4

; <label>:4:                                      ; preds = %1
  %5 = mul nsw i32 %2, %0
  %6 = sdiv i32 %5, 4
  br label %7

; <label>:7:                                      ; preds = %1, %4
  %8 = phi i32 [ %6, %4 ], [ 0, %1 ]
  ret i32 %8
}

; Function Attrs: nounwind uwtable
define i32 @_Z13__omp_for_endi(i32) local_unnamed_addr #0 {
  %2 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2) #1
  %3 = icmp eq i32 %2, 3
  br i1 %3, label %8, label %4

; <label>:4:                                      ; preds = %1
  %5 = add nsw i32 %2, 1
  %6 = mul nsw i32 %5, %0
  %7 = sdiv i32 %6, 4
  br label %8

; <label>:8:                                      ; preds = %1, %4
  %9 = phi i32 [ %7, %4 ], [ %0, %1 ]
  ret i32 %9
}

; Function Attrs: nounwind uwtable
define void @_Z25__omp_assign_float_reductPff(float* nocapture, float) local_unnamed_addr #0 {
  %3 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2) #1
  %4 = getelementptr inbounds float, float* %0, i32 %3
  store float %1, float* %4, align 4, !tbaa !1
  ret void
}

; Function Attrs: nounwind uwtable
define void @_Z23__omp_assign_int_reductPii(i32* nocapture, i32) local_unnamed_addr #0 {
  %3 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2) #1
  %4 = getelementptr inbounds i32, i32* %0, i32 %3
  store i32 %1, i32* %4, align 4, !tbaa !5
  ret void
}

; Function Attrs: nounwind uwtable
define void @_Z23__omp_float_reduct_plusPfS_(float* nocapture readonly, float* nocapture) local_unnamed_addr #0 {
  %3 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2) #1
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %5, label %17

; <label>:5:                                      ; preds = %2
  store float 0.000000e+00, float* %1, align 4, !tbaa !1
  %6 = load float, float* %0, align 4, !tbaa !1
  %7 = fadd float %6, 0.000000e+00
  store float %7, float* %1, align 4, !tbaa !1
  %8 = getelementptr inbounds float, float* %0, i32 1
  %9 = load float, float* %8, align 4, !tbaa !1
  %10 = fadd float %9, %7
  store float %10, float* %1, align 4, !tbaa !1
  %11 = getelementptr inbounds float, float* %0, i32 2
  %12 = load float, float* %11, align 4, !tbaa !1
  %13 = fadd float %12, %10
  store float %13, float* %1, align 4, !tbaa !1
  %14 = getelementptr inbounds float, float* %0, i32 3
  %15 = load float, float* %14, align 4, !tbaa !1
  %16 = fadd float %15, %13
  store float %16, float* %1, align 4, !tbaa !1
  br label %17

; <label>:17:                                     ; preds = %5, %2
  ret void
}

; Function Attrs: nounwind uwtable
define void @_Z21__omp_int_reduct_plusPiS_(i32* nocapture readonly, i32* nocapture) local_unnamed_addr #0 {
  %3 = tail call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg(i32 2) #1
  %4 = icmp eq i32 %3, 0
  br i1 %4, label %5, label %16

; <label>:5:                                      ; preds = %2
  store i32 0, i32* %1, align 4, !tbaa !5
  %6 = load i32, i32* %0, align 4, !tbaa !5
  store i32 %6, i32* %1, align 4, !tbaa !5
  %7 = getelementptr inbounds i32, i32* %0, i32 1
  %8 = load i32, i32* %7, align 4, !tbaa !5
  %9 = add nsw i32 %6, %8
  store i32 %9, i32* %1, align 4, !tbaa !5
  %10 = getelementptr inbounds i32, i32* %0, i32 2
  %11 = load i32, i32* %10, align 4, !tbaa !5
  %12 = add nsw i32 %9, %11
  store i32 %12, i32* %1, align 4, !tbaa !5
  %13 = getelementptr inbounds i32, i32* %0, i32 3
  %14 = load i32, i32* %13, align 4, !tbaa !5
  %15 = add nsw i32 %12, %14
  store i32 %15, i32* %1, align 4, !tbaa !5
  br label %16

; <label>:16:                                     ; preds = %5, %2
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @_Z18__omp_barrier_initP15__omp_barrier_ti(%struct.__omp_barrier_t* nocapture, i32) local_unnamed_addr #3 {
  %3 = getelementptr inbounds %struct.__omp_barrier_t, %struct.__omp_barrier_t* %0, i32 0, i32 0
  store i32 %1, i32* %3, align 4, !tbaa !7
  ret void
}

; Function Attrs: nounwind uwtable
define void @_Z18__omp_barrier_waitP15__omp_barrier_t(%struct.__omp_barrier_t* nocapture readonly) local_unnamed_addr #0 {
  %2 = getelementptr inbounds %struct.__omp_barrier_t, %struct.__omp_barrier_t* %0, i32 0, i32 0
  %3 = load i32, i32* %2, align 4, !tbaa !7
  tail call void @llvm.nuplus.__builtin_nuplus_barrier_core(i32 %3, i32 0)
  ret void
}

; Function Attrs: nounwind
declare void @llvm.nuplus.__builtin_nuplus_barrier_core(i32, i32) #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { norecurse nounwind readnone uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { norecurse nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.0 (tags/RELEASE_390/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"float", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"int", !3, i64 0}
!7 = !{!8, !6, i64 0}
!8 = !{!"_ZTS15__omp_barrier_t", !6, i64 0}
