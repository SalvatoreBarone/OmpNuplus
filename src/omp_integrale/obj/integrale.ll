; ModuleID = 'integrale.cpp'
source_filename = "integrale.cpp"
target datalayout = "e-m:e-p:32:32"
target triple = "nuplus-none-none"

%struct.parallel_region_0_args_t = type { float, [4 x float], %struct.__omp_barrier_t, %struct.__omp_barrier_t }
%struct.__omp_barrier_t = type { i32 }

@a = local_unnamed_addr global float 0.000000e+00, align 4
@b = local_unnamed_addr global float 3.000000e+00, align 4
@n_int = local_unnamed_addr global i32 1000, align 4
@h = local_unnamed_addr global float 0x3F689374C0000000, align 4
@partial = global float 0.000000e+00, align 4
@parallel_region_0_args = global %struct.parallel_region_0_args_t zeroinitializer, align 4
@llvm.global_ctors = appending global [0 x { i32, void ()*, i8* }] zeroinitializer

; Function Attrs: nounwind uwtable
define void @_Z17parallel_region_0Pv(i8*) local_unnamed_addr #0 {
  %2 = bitcast i8* %0 to float*
  %3 = load float, float* %2, align 4, !tbaa !1
  %4 = getelementptr inbounds i8, i8* %0, i32 20
  %5 = bitcast i8* %4 to %struct.__omp_barrier_t*
  tail call void @_Z18__omp_barrier_waitP15__omp_barrier_t(%struct.__omp_barrier_t* %5) #4
  %6 = load i32, i32* @n_int, align 4, !tbaa !8
  %7 = tail call i32 @_Z15__omp_for_starti(i32 %6) #4
  %8 = load i32, i32* @n_int, align 4, !tbaa !8
  %9 = tail call i32 @_Z13__omp_for_endi(i32 %8) #4
  %10 = icmp slt i32 %7, %9
  br i1 %10, label %11, label %30

; <label>:11:                                     ; preds = %1
  br label %12

; <label>:12:                                     ; preds = %11, %12
  %13 = phi float [ %24, %12 ], [ %3, %11 ]
  %14 = phi i32 [ %25, %12 ], [ %7, %11 ]
  %15 = load float, float* @a, align 4, !tbaa !9
  %16 = sitofp i32 %14 to float
  %17 = load float, float* @h, align 4, !tbaa !9
  %18 = fmul float %16, %17
  %19 = fadd float %15, %18
  %20 = fpext float %19 to double
  %21 = tail call double @sin(double %20) #4
  %22 = fpext float %13 to double
  %23 = fadd double %22, %21
  %24 = fptrunc double %23 to float
  %25 = add nsw i32 %14, 1
  %26 = load i32, i32* @n_int, align 4, !tbaa !8
  %27 = tail call i32 @_Z13__omp_for_endi(i32 %26) #4
  %28 = icmp slt i32 %25, %27
  br i1 %28, label %12, label %29

; <label>:29:                                     ; preds = %12
  br label %30

; <label>:30:                                     ; preds = %29, %1
  %31 = phi float [ %3, %1 ], [ %24, %29 ]
  %32 = getelementptr inbounds i8, i8* %0, i32 4
  %33 = bitcast i8* %32 to float*
  %34 = load float, float* @h, align 4, !tbaa !9
  %35 = fmul float %31, %34
  tail call void @_Z25__omp_assign_float_reductPff(float* %33, float %35) #4
  %36 = getelementptr inbounds i8, i8* %0, i32 24
  %37 = bitcast i8* %36 to %struct.__omp_barrier_t*
  tail call void @_Z18__omp_barrier_waitP15__omp_barrier_t(%struct.__omp_barrier_t* %37) #4
  tail call void @_Z23__omp_float_reduct_plusPfS_(float* %33, float* nonnull @partial) #4
  ret void
}

declare void @_Z18__omp_barrier_waitP15__omp_barrier_t(%struct.__omp_barrier_t*) local_unnamed_addr #1

declare i32 @_Z15__omp_for_starti(i32) local_unnamed_addr #1

declare i32 @_Z13__omp_for_endi(i32) local_unnamed_addr #1

; Function Attrs: nounwind
declare double @sin(double) local_unnamed_addr #2

declare void @_Z25__omp_assign_float_reductPff(float*, float) local_unnamed_addr #1

declare void @_Z23__omp_float_reduct_plusPfS_(float*, float*) local_unnamed_addr #1

; Function Attrs: norecurse nounwind uwtable
define i32 @main() local_unnamed_addr #3 {
  %1 = tail call i32 @_Z19__omp_get_thread_idv() #4
  %2 = icmp eq i32 %1, 0
  br i1 %2, label %3, label %15

; <label>:3:                                      ; preds = %0
  store float 0.000000e+00, float* getelementptr inbounds (%struct.parallel_region_0_args_t, %struct.parallel_region_0_args_t* @parallel_region_0_args, i32 0, i32 0), align 4, !tbaa !1
  %4 = tail call i32 @_Z21__omp_get_num_threadsv() #4
  %5 = icmp sgt i32 %4, 0
  br i1 %5, label %6, label %8

; <label>:6:                                      ; preds = %3
  br label %9

; <label>:7:                                      ; preds = %9
  br label %8

; <label>:8:                                      ; preds = %7, %3
  tail call void @_Z18__omp_barrier_initP15__omp_barrier_ti(%struct.__omp_barrier_t* getelementptr inbounds (%struct.parallel_region_0_args_t, %struct.parallel_region_0_args_t* @parallel_region_0_args, i32 0, i32 2), i32 0) #4
  tail call void @_Z18__omp_barrier_initP15__omp_barrier_ti(%struct.__omp_barrier_t* getelementptr inbounds (%struct.parallel_region_0_args_t, %struct.parallel_region_0_args_t* @parallel_region_0_args, i32 0, i32 3), i32 1) #4
  br label %15

; <label>:9:                                      ; preds = %6, %9
  %10 = phi i32 [ %12, %9 ], [ 0, %6 ]
  %11 = getelementptr inbounds %struct.parallel_region_0_args_t, %struct.parallel_region_0_args_t* @parallel_region_0_args, i32 0, i32 1, i32 %10
  store float 0.000000e+00, float* %11, align 4, !tbaa !9
  %12 = add nuw nsw i32 %10, 1
  %13 = tail call i32 @_Z21__omp_get_num_threadsv() #4
  %14 = icmp slt i32 %12, %13
  br i1 %14, label %9, label %7

; <label>:15:                                     ; preds = %8, %0
  tail call void @_Z17parallel_region_0Pv(i8* bitcast (%struct.parallel_region_0_args_t* @parallel_region_0_args to i8*))
  ret i32 0
}

declare i32 @_Z19__omp_get_thread_idv() local_unnamed_addr #1

declare i32 @_Z21__omp_get_num_threadsv() local_unnamed_addr #1

declare void @_Z18__omp_barrier_initP15__omp_barrier_ti(%struct.__omp_barrier_t*, i32) local_unnamed_addr #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { norecurse nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.0 (tags/RELEASE_390/final)"}
!1 = !{!2, !3, i64 0}
!2 = !{!"_ZTS24parallel_region_0_args_t", !3, i64 0, !4, i64 4, !6, i64 20, !6, i64 24}
!3 = !{!"float", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!"_ZTS15__omp_barrier_t", !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!7, !7, i64 0}
!9 = !{!3, !3, i64 0}
