/**
 * @file integrale.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 *
 * This is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "__omp.hpp"	// __omp.hpp contains some openMP-like functions
#include <math.h>

/*
In this example we will show how to translate an openMP program that uses the reduction clauses.
Consider the following piece of code:

	sequential_part();
	int i;
	float tmp = 0;
	#pragma omp parallel \
		default(none) \
		shared(a, b, h, n_int, func) \
		private (i) \
		firstprivate(tmp) \
		reduction(+:partial)
	{
		#pragma omp for private(i)
			for (i = 0; i < (int)n_int; i++)
				tmp += func(a + float(i * h));
			partial = tmp * h;
	}
	another_sequential_part();
*/

/*
As in the previous examples, shared variables are translated as globally defined variables.
*/
float a = 0, b = 3;
int n_int = 1000;
float h = (b - a) / n_int;
/*
The tmp variable is a firstprivate variable, so, in the
parallel_region_0_args_t there is a variable with same name, time and size.
The reduction clause acts on "partial" variable. In our implementation we will use an array with the same name and the same type of variable,
used by each of the threads to save the result of its calculation to a reserved location. The master-thread will use this array to make the reduction.
From now, we call this array as "reduction array".
*/
float partial = 0;
typedef struct  {
	float tmp;
	float partial[__OMP_NUM_THREADS];
	__omp_barrier_t start_barrier;
	__omp_barrier_t end_barrier;
} parallel_region_0_args_t;

/*
The parallel_region_0_args_t variable must be globally defined, so its life will span the entire program life-time,
and initialized. Ignore the two __omp_barrier_t, in this moment. The "tmp" and "partial" variable must be initialized with
the value of their original counterparts.
*/
parallel_region_0_args_t parallel_region_0_args;

/*
Let's look at the parallel region now.
There is a local variable called tmp, initialized with the value passed to the function through the parallel_region_0_args_t struct.
The __omp_for_start() and __omp_for_end() functions are used to partition the outer for-cycle to each thread.
To assign a value to the thread's reserved location in the "reduction array", each thread must use the __omp_assign_T_reduct, there T
is the type of the variable, so __omp_assign_float_reduct() can be used to assign a float, __omp_assign_int_reduct() can be used to
assign an integer, and so on.
Near the end of the parallel section there is a call to __omp_barrier(). Threads need to sync each-other and wait for everyone to
finish running before they can continue.
After the sync-barrier there is a call to __omp_float_reduct_plus() function. The operation specifide inside this function will be
executed only by the master-thread, to make the reduction.
*/
void parallel_region_0(void *args) {
	parallel_region_0_args_t *_args = (parallel_region_0_args_t*) args;
	int i;
	float tmp = _args->tmp;

	__omp_barrier_wait(&_args->start_barrier);

	for (i = __omp_for_start(n_int); i < __omp_for_end(n_int); i++)
		tmp += sin(a + float(i * h));
	__omp_assign_float_reduct(_args->partial, (tmp*h));

	__omp_barrier_wait(&_args->end_barrier);

	__omp_float_reduct_plus(_args->partial, &partial);
}

int main(){


if (__omp_get_thread_id() == __OMP_MASTER_THREAD) {
	parallel_region_0_args.tmp = 0;
	for (int c = 0; c < __omp_get_num_threads(); c++)
		parallel_region_0_args.partial[c] = 0;
	__omp_barrier_init(&parallel_region_0_args.start_barrier, 0);
	__omp_barrier_init(&parallel_region_0_args.end_barrier, 1);
}

	parallel_region_0((void*)&parallel_region_0_args);

	return 0;
}
