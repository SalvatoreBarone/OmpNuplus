/**
 * @file __omp.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 *
 * This is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include "__omp.hpp"

#ifdef __PTHREAD__
#include <unistd.h>
#include <sys/syscall.h>
#endif

#define TILEID_CTRL_REG 	0
#define COREID_CTRL_REG 	1
#define THREADID_CTRL_REG	2

/**
 * @brief Get the hardware-thread id.
 * @return hardware-thread id, based only on Thread-id control register.
 */
long __omp_get_thread_id() {
#ifdef __PTHREAD__
	return syscall(SYS_gettid) - getpid();
#else
	return __builtin_nuplus_read_control_reg(THREADID_CTRL_REG);
#endif
}

/**
 * @brief Get the number of threads that will run a parallel region.
 *
 * In this pseudo-implementation, it returns the value defined by __OMP_NUM_THREADS.
 *
 * @return the number of threads that will run a parallel region.
 */
long __omp_get_num_threads(void) {
	return __OMP_NUM_THREADS;
}

/**
 * @brief Statring index computatuon for partitioning of a paraller-for loop.
 *
 * @warning This implementation works only with "for(i=0; i<N;i++)"-type for loop
 *
 * @param [in] upperbound upperbound of the for loop
 *
 * @return the starting index for the loop, for the current thread
 */
int __omp_for_start(int upperbound) {
	int id = __omp_get_thread_id();
	int threads = __omp_get_num_threads();
	return (id == 0 ? 0 : id*(upperbound)/threads);
}

/**
 * @brief Ending index computatuon for partitioning of a paraller-for loop.
 *
 * @warning This implementation works only with "for(i=0; i<N;i++)"-type for loop
 *
 * @param [in] upperbound upperbound of the for loop
 *
 * @return the ending index for the loop, for the current thread
 */
int __omp_for_end(int upperbound) {
	int id = __omp_get_thread_id();
	int threads = __omp_get_num_threads();
	return (id == threads-1 ? upperbound : (id+1)*(upperbound)/threads);
}



/**
 * @brief Allows a thread to assign a value to its reserved location in the reduction array,
 *
 * @param [in,out] reduct_array pointer to the reduction array
 * @param [in] value value to be assigned to the reserved lease
 */
void __omp_assign_float_reduct (float* reduct_array, float value) {
	int id = __omp_get_thread_id();
	reduct_array[id] = value;
}

/**
 * @brief Allows a thread to assign a value to its reserved location in the reduction array,
 *
 * @param [in,out] reduct_array pointer to the reduction array
 * @param [in] value value to be assigned to the reserved lease
 */
void __omp_assign_int_reduct (int* reduct_array, int value) {
	int id = __omp_get_thread_id();
	reduct_array[id] = value;
}

/**
 * @brief Operates a reduction on a reduction-array, using the + operator
 *
 * @param [in] reduct_array pointer to the reduction array
 * @param [out] result puntatore alla variabile che dovrà contenere il risultato della reduction
 */
void __omp_float_reduct_plus (float* reduct_array, float* result) {
	if (__omp_get_thread_id() == __OMP_MASTER_THREAD) {
		int i;
		*result = 0;
		for (i = 0; i < __OMP_NUM_THREADS; i++)
			*result += reduct_array[i];
	}
}

/**
 * @brief Operates a reduction on a reduction-array, using the + operator
 *
 * @param [in] reduct_array pointer to the reduction array
 * @param [out] result puntatore alla variabile che dovrà contenere il risultato della reduction
 */
void __omp_int_reduct_plus (int* reduct_array, int* result) {
	if (__omp_get_thread_id() == __OMP_MASTER_THREAD) {
		int i;
		*result = 0;
		for (i = 0; i < __OMP_NUM_THREADS; i++)
			*result += reduct_array[i];
	}
}

#ifdef __PTHREAD__
void __omp_barrier_init (__omp_barrier_t *barrier) {
	pthread_barrier_init(barrier, NULL, __OMP_NUM_THREADS);
}
#else
void __omp_barrier_init (__omp_barrier_t *barrier, int id) {
	barrier->id = id;
}
#endif

/**
 * @brief Sends a message "I arrived at the sync barrier" to the synchronization barrier manager.
 *
 * The thread is suspended until all the other threads have reached the same synchronization barrier.
 *
 * @param [in] barrier pointer to __omp_barrier_t, containing barrier-id
 */
void __omp_barrier_wait(__omp_barrier_t *barrier) {
#ifdef __PTHREAD__
	pthread_barrier_wait(barrier);
#else
	__builtin_nuplus_barrier_core(barrier->id, 0);
#endif
}
