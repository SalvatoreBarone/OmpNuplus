/**
 * @file integrale_pthread.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 *
 * This is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include <iostream>
#include <iomanip>
#include <pthread.h>

#include "__omp.hpp"	// __omp.hpp contains some openMP-like functions
#include <math.h>

using namespace std;

float a = 0, b = 3;
int n_int = 1000;
float h = (b - a) / n_int;
float partial = 0;

typedef struct  {
	float tmp;
	float partial[__OMP_NUM_THREADS];
	__omp_barrier_t start_barrier;
	__omp_barrier_t end_barrier;
} parallel_region_0_args_t;

parallel_region_0_args_t parallel_region_0_args;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t thread_id[__OMP_NUM_THREADS-1];

void* parallel_region_0(void *args) {
	parallel_region_0_args_t *_args = (parallel_region_0_args_t*) args;
	int i;
	float tmp = _args->tmp;

	__omp_barrier_wait(&_args->start_barrier);

	for (i = __omp_for_start(n_int); i < __omp_for_end(n_int); i++)
		tmp += sin(a + float(i * h));
	__omp_assign_float_reduct(_args->partial, (tmp*h));

	__omp_barrier_wait(&_args->end_barrier);

	__omp_float_reduct_plus(_args->partial, &partial);

	return NULL;
}

int main(){

	// master-thread only code-section
	parallel_region_0_args.tmp = 0;
	for (int c = 0; c < __omp_get_num_threads(); c++)
		parallel_region_0_args.partial[c] = 0;
#ifdef __PTHREAD__
	__omp_barrier_init(&parallel_region_0_args.start_barrier);
	__omp_barrier_init(&parallel_region_0_args.end_barrier);
#else
	__omp_barrier_init(&parallel_region_0_args.start_barrier, 0);
	__omp_barrier_init(&parallel_region_0_args.end_barrier, 1);
#endif


	// begin of the parallel section
	for (int i = 0; i < __OMP_NUM_THREADS-1; i++)
		pthread_create(&thread_id[i], NULL, parallel_region_0, (void*)&parallel_region_0_args);

	parallel_region_0((void*)&parallel_region_0_args);

	for (int i = 0; i < __OMP_NUM_THREADS-1; i++)
		pthread_join(thread_id[i], NULL);
	// end of the parallel region

	cout << partial << endl;


	return 0;
}
