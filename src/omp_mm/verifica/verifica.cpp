#include <iostream>
#include <iomanip>
#include "omp.h"

using namespace std;

#include "data.hpp"

int main() {

	int i, j, k, tmp;
	#pragma omp parallel \
	 	default(none) \
		private(i, j, k, tmp) \
		shared (A, B, C, rows, cols)
	{
		#pragma omp parallel for \
			private(i, j, k, tmp) \
			shared (A, B, C, rows, cols)
		for (i=0; i<rows; i++)
			for (j=0; j<cols; j++) {
				tmp = 0;
				for (k=0; k<cols; k++)
					tmp += A[i][k]*B[k][j];
				C[i][j] = tmp;
			}
	}

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++)
#ifdef __PRINT_HEX__
			cout << hex << C[i][j] << "\t";
#else
			cout << C[i][j] << "\t";
#endif
		cout << endl;
	}
}
