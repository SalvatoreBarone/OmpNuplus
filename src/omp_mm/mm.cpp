/**
 * @file mm.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 *
 * This is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */



/*
Usually, in a system with OS and thread library, openMP directives will be transtated with explicit call to OS thread
library functions. nu+ hasn't any OS, neither thread library, so translation must be done "manually". As we will see,
the translation procedure is very simple and easily automated. The following examples shows how an openMP program can be
translated in an openMP-like fashion with only a little effort.
*/

#include "__omp.hpp"	// __omp.hpp contains some openMP-like functions
#include "data.hpp"		// data.hpp contains data that will be processed by this program


/*
Consider the following piece of code:

	int i, j, k, tmp;
	#pragma omp parallel \
		default(none) \
		private(i, j, k, tmp) \
		shared (A, B, C, rows, cols)
	{
		#pragma omp parallel for \
			private(i, j, k, tmp) \
			shared (A, B, C, rows, cols)
		for (i=0; i<rows; i++)
			for (j=0; j<cols; j++) {
				tmp = 0;
				for (k=0; k<cols; k++)
					tmp += A[i][k]*B[k][j];
				C[i][j] = tmp;
			}
	}

First of all, using __omp_get_thread_id() we will be able to distinguish between threads, so senquential parts can be
enclosed in an if-then statement. All functions with the "__omp" prefix are written ad-hoc, to simulate the presence of
the openMP library.

Eeach line in the parallel section will be enclosed in a subrourine like "void parallel_region_X(void*)" where X is the
serial identification number of the parallel section. If we have two parallel section, the first one will be enclosed in
parallel_region_0() then the second one will be enclosed in parallel_region_1(). In this case there is only one parallel
region, so...
*/
void parallel_region_0(void* args);

/*
Let's consider variables used in the parallel section. All shared variables will be passed to each thread through their
memory address, in a struct. For example

	typedef struct {
		...
	} parallel_region_X_args_t;

will contain the memory-address for each shared variable. In addition, the structure must contain the value to be
assigned to the firstprivate variables. Variables marked as private, instead, will be defined internally to the
subroutine that encapsulates the parallel region.
If shared variables are defined as global variable there is no need to pass them to each thread through the struct
defined above. This time we'll take the second solution, so we define the following struct.
*/
typedef struct  {
	__omp_barrier_t start_barrier;
	__omp_barrier_t end_barrier;
} parallel_region_0_args_t;

/*
The parallel_region_0_args_t variable must be globally defined, so its life will span the entire program life-time,
and initialized. Ignore the two __omp_barrier_t, in this moment.
*/
parallel_region_0_args_t parallel_region_0_args;

/*
Let's look at the main now. It's easy to understand, even without any word.
Each line before the beginning of parallel section, those which must be executed by master-thread only, are
enclosed in an if-then statement. Same thing with line immediately after the end of parallel section.
*/
int main(){
	// master-thread only code-section
	if (__omp_get_thread_id() == __OMP_MASTER_THREAD) {
		__omp_barrier_init(&parallel_region_0_args.start_barrier, 0);
		__omp_barrier_init(&parallel_region_0_args.end_barrier, 1);
	}
	// begin of parallel region 0
	parallel_region_0((void*)&parallel_region_0_args);
	// end of parallel region 0
	// master-thread only code-section
	if (__omp_get_thread_id() == __OMP_MASTER_THREAD) {

	}
	return 0;
}

/*
Let's look at the parallel section, enclosed in the parallel_region_0() subroutine.
It starts with the declaration of i, the variable marked as private in the parallel section.

Note that if shared variables are defined as global variable, there is no need to pass them to
each thread through the struct. Also there is no need to put an "_args->" prefix in front of them.

The first call to __omp_barrier() is used to stop all other threads until the master-thread reaches the parallel region.
__omp_for_start() and __omp_for_end() is used to partition the outer for-cycle to each thread.
Before the end of the parallel section there is another call to __omp_barrier(). Threads need to sync each-other and
wait for everyone to finish running before they can continue.
*/
void parallel_region_0(void *args) {
	parallel_region_0_args_t *_args = (parallel_region_0_args_t*) args;
	int i, j, k, tmp;
	__omp_barrier_wait(&_args->start_barrier);
	for (i=__omp_for_start(rows); i<__omp_for_end(rows); i++)
		for (j=0; j<cols; j++) {
			tmp = 0;
			for (k=0; k<cols; k++)
				tmp += A[i][k]*B[k][j];
			C[i][j] = tmp;
		}
	__omp_barrier_wait(&_args->end_barrier);
}
