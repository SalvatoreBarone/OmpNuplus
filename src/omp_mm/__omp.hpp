/**
 * @file __omp.hpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 *
 * This is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#ifndef __omp_simple_implementation_h__
#define __omp_simple_implementation_h__

#ifdef __PTHREAD__
#include <pthread.h>
#endif

#define __OMP_NUM_THREADS	4
#define __OMP_MASTER_THREAD	0
#define __OMP_INIT_REDUCTION_ARRAY {0, 0, 0, 0}

long	__omp_get_num_threads		(void);
long	__omp_get_thread_id			(void);

int		__omp_for_start				(int upperbound);
int		__omp_for_end				(int upperbound);

void 	__omp_assign_float_reduct	(float* reduct_array, float value);
void 	__omp_assign_int_reduct		(int* reduct_array, int value);

void 	__omp_float_reduct_plus		(float* reduct_array, float* result);
void 	__omp_int_reduct_plus		(int* reduct_array, int* result);

#ifdef __PTHREAD__
typedef pthread_barrier_t __omp_barrier_t;
#else
typedef struct {
	int id;
} __omp_barrier_t;
#endif

#ifdef __PTHREAD__
void	__omp_barrier_init			(__omp_barrier_t *barrier);
#else
void	__omp_barrier_init			(__omp_barrier_t *barrier, int id);
#endif
void	__omp_barrier_wait			(__omp_barrier_t *barrier);

#endif
