/**
 * @file complesso_pthread.cpp
 * @author Salvatore Barone <salvator.barone@gmail.com>
 *
 * @copyright
 * Copyright 2017 Salvatore Barone <salvator.barone@gmail.com>
 *
 *
 * This is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either version 3 of
 * the License, or any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not,
 * write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
 * USA.
 */

#include <iostream>
#include <iomanip>
#include <pthread.h>

using namespace std;

#include "__omp.hpp"	// __omp.hpp contains some openMP-like functions
#include "data.hpp"		// data.hpp contains data that will be processed by this program


void* parallel_region_0(void* args);

typedef struct  {
	__omp_barrier_t start_barrier;
	__omp_barrier_t end_barrier;
} parallel_region_0_args_t;

parallel_region_0_args_t parallel_region_0_args;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t thread_id[__OMP_NUM_THREADS-1];

int main(){
	// master-thread only code-section
#ifdef __PTHREAD__
	__omp_barrier_init(&parallel_region_0_args.start_barrier);
	__omp_barrier_init(&parallel_region_0_args.end_barrier);
#else
	__omp_barrier_init(&parallel_region_0_args.start_barrier, 0);
	__omp_barrier_init(&parallel_region_0_args.end_barrier, 1);
#endif

	for (int i = 0; i < __OMP_NUM_THREADS-1; i++)
		pthread_create(&thread_id[i], NULL, parallel_region_0, (void*)&parallel_region_0_args);

	parallel_region_0((void*)&parallel_region_0_args);

	for (int i = 0; i < __OMP_NUM_THREADS-1; i++)
		pthread_join(thread_id[i], NULL);

	cout << endl;
	cout << endl;
	cout << endl;
	for (int i = 0; i < n; i ++) {
		cout << Re_R[i] << "\t";
		if ((i+1) % 16 == 0)
			cout << endl;
	}

	cout << endl;
	cout << endl;
	cout << endl;
	for (int i = 0; i < n; i ++) {
		cout << Im_R[i] << "\t";
		if ((i+1) % 16 == 0)
			cout << endl;
	}
	cout << endl;


	return 0;
}


void* parallel_region_0(void *args) {
	parallel_region_0_args_t *_args = (parallel_region_0_args_t*) args;
	int i;

	__omp_barrier_wait(&_args->start_barrier);

	for (i = __omp_for_start(n); i < __omp_for_end(n); i++) {
		Re_R[i] = Re_A[i] * Re_B[i] - Im_A[i] * Im_B[i];
		Im_R[i] = Re_A[i] * Im_B[i] + Im_A[i] * Re_B[i];
	}
	__omp_barrier_wait(&_args->end_barrier);

	return NULL;
}
